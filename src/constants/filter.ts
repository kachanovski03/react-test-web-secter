export enum FILTER_TYPE {
    RANGE = "range",
    CHECKBOX = "checkbox",
    MULTIPLE = "multiple",
}
