import React from 'react';
import './App.css';
import {Preloader} from "./compoennts/preloader/Preloader";
import {useSelector} from "react-redux";
import {RootStateType} from "./redux/store";
import { Pages } from './routes';

export const App = () => {

    const isFetching = useSelector<RootStateType, boolean>(state => state.common.isFetching)

    return (
        <>
            <Preloader isFetching={isFetching} />
            <Pages/>
        </>

    );
}
