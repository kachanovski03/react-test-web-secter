import {catalogReducer} from "./catalog";
import {commonReducer} from "./common";
import {categoriesReducer} from "./categories";

import type {ICatalogReducer} from "./catalog";
import type {ICommonReducer} from "./common";
import type {ICategoryReducer} from "./categories";


export {
    catalogReducer,
    commonReducer,
    categoriesReducer,

}

export type {
    ICatalogReducer,
    ICommonReducer,
    ICategoryReducer
}
