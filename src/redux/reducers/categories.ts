import {InferActionsType} from "../store";
import {SET_CATEGORIES} from "../constants";
import {ICategory} from "../../interface";
import {categoriesActions} from "../actions/categories";

export type IActions = InferActionsType<typeof categoriesActions>
export type ICategoryReducer = typeof defaultState;

const defaultState = {
    categories: [] as ICategory[]
}

export const categoriesReducer = (state = defaultState, action: IActions): ICategoryReducer => {
    switch (action.type) {
        case SET_CATEGORIES:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}
