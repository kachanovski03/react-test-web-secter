import {InferActionsType} from "../store";
import {catalogActions} from "../actions";
import {
    SET_CATALOG_CATEGORIES,
    SET_CATALOG_FILTERS,
    SET_CATALOG_META,
    SET_CATALOG_PRODUCTS,
    SET_CURRENT_CATEGORY
} from "../constants";
import {ICategory, IFilter, IItem, IMetaInfo} from "../../interface";

export type IActions = InferActionsType<typeof catalogActions>
export type ICatalogReducer = typeof defaultState;

const defaultState = {
    products: [] as IItem[],
    filters: [] as IFilter[],
    meta: null as (IMetaInfo | null),
    categories: [] as ICategory[],
    currentCategory: null as (ICategory | null)
}

export const catalogReducer = (state = defaultState, action: IActions): ICatalogReducer => {
    switch (action.type) {
        case SET_CATALOG_PRODUCTS:
        case SET_CATALOG_META:
        case SET_CATALOG_FILTERS:
        case SET_CATALOG_CATEGORIES:
        case SET_CURRENT_CATEGORY:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}
