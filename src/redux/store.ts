import {Action, applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleWare, {ThunkAction} from "redux-thunk";
import {catalogReducer, categoriesReducer, commonReducer} from "./reducers";

let reducers = combineReducers({
    catalog: catalogReducer,
    common: commonReducer,
    categories: categoriesReducer,
})

export const store = createStore(reducers, applyMiddleware(thunkMiddleWare))

type PropertiesType<T> = T extends { [key: string]: infer U } ? U : never;
export type InferActionsType<T extends { [key: string]: (...args: any[]) => any }> = ReturnType<PropertiesType<T>>;

export type BaseThunkType<A extends Action, R = Promise<void>> = ThunkAction<R,
    RootStateType,
    unknown,
    A>;

export type RootStateType = ReturnType<typeof reducers>

// @ts-ignore
window.store = store
