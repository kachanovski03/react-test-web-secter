import {catalogActions} from "./catalog";
import {commonActions} from "./common";

export {catalogActions, commonActions}
