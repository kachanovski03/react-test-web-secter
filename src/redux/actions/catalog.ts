import {
    SET_CATALOG_CATEGORIES,
    SET_CATALOG_FILTERS,
    SET_CATALOG_META,
    SET_CATALOG_PRODUCTS,
    SET_CURRENT_CATEGORY
} from "../constants";
import {ICategory, IFilter, IItem, IMetaInfo} from "../../interface";

export const catalogActions = {
    setCatalogProducts: (products: IItem[]) => ({type: SET_CATALOG_PRODUCTS, payload: {products}} as const),
    setCatalogFilters: (filters: IFilter[]) => ({type: SET_CATALOG_FILTERS, payload: {filters}} as const),
    setCatalogMeta: (meta: IMetaInfo | null) => ({type: SET_CATALOG_META, payload: {meta}} as const),
    setCatalogCategories: (categories: ICategory[]) => ({type: SET_CATALOG_CATEGORIES, payload: {categories}} as const),
    setCurrentCategory: (currentCategory: ICategory | null) => ({type: SET_CURRENT_CATEGORY, payload: {currentCategory}} as const),
};
