import {SET_CATEGORIES} from "../constants";
import {ICategory} from "../../interface";

export const categoriesActions = {
    setCategories: (categories: ICategory[]) => ({type: SET_CATEGORIES, payload: {categories}} as const),
};
