import {commonThunkCreator} from "../utils";
import {BaseThunkType, InferActionsType} from "../store";
import {categoriesActions} from "../actions/categories";
import {categoriesAPI} from "../../api/categories";

type ThunkType = BaseThunkType<InferActionsType<typeof categoriesActions>>;

export const getCategories = (): ThunkType => async dispatch => {
    await commonThunkCreator(async () => {
        const res = await categoriesAPI.getCategories()
        dispatch(categoriesActions.setCategories(res.categories))
    }, dispatch);
};
