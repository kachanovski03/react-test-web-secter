import {commonThunkCreator} from "../utils";
import {catalogAPI} from "../../api";
import {catalogActions} from "../actions";
import {BaseThunkType, InferActionsType} from "../store";

type ThunkType = BaseThunkType<InferActionsType<typeof catalogActions>>;

export const getCatalogData = (category: string,queryUrl?: string): ThunkType => async dispatch => {
    await commonThunkCreator(async () => {
        const res = await catalogAPI.getCatalog(category, queryUrl)
        dispatch(catalogActions.setCatalogProducts(res.products))
        dispatch(catalogActions.setCatalogFilters(res.filters))
        dispatch(catalogActions.setCatalogMeta(res.meta))
        dispatch(catalogActions.setCatalogCategories(res.categories))
        dispatch(catalogActions.setCurrentCategory(res.categories.find((i: any) => i.slug === category)))
    }, dispatch);
};
