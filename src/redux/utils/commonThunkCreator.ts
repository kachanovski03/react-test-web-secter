import { commonActions } from "../actions";

export const commonThunkCreator = (operation: any, dispatch: any) => {
    let tryCathed = withTryCath(operation, dispatch);
    let isFetching = withIsFetching(tryCathed, dispatch);
    return isFetching();
};

const withTryCath = (operation: any, dispatch: any) => {
    return async () => {
        try {
            return await operation();
        } catch (e: any) {
            if(e.response.data.error === true){
                dispatch(commonActions.setStatus(`${e.response.data.message}`))
            } else {
                dispatch(commonActions.setStatus(`Что то пошло не так`))
            }
        }
        return null;
    };
};

const withIsFetching = (operation: any, dispatch: any) => {
    return async () => {
        dispatch(commonActions.setIsFetching(true));
        await operation();
        dispatch(commonActions.setIsFetching(false));
    };
};
