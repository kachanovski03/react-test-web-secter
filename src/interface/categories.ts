import {IItemImageType} from "./catalog";

export interface ICategory {
    children: any[]
    icon: string
    id: number
    image: IItemImageType
    slug: string
    title: string
}
