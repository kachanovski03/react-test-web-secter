import {FILTER_TYPE} from "../constants";

export interface IItem {
    category: ICategoryInfo
    condition: number
    discount_trade_in: string
    id: number
    image: IItemImageType
    in_stock: boolean
    is_new: boolean
    is_second_hand: boolean
    price: string
    slug: string
    title: string
}

export interface ICategoryInfo {
    id: number
    slug: string
    title: string
}

export interface IItemImageType {
    desktop: IImageType
    mobile: IImageType
    tablet: IImageType
}

export interface IImageType {
    webp_x1: string,
    webp_x2: string,
    x1: string,
    x2: string
}

export interface IFilter {
    info: null | string
    items: IFilterItem[]
    max: string
    min: string
    slug: string
    title: string
    type: FILTER_TYPE
    initialValue?: string
}

export interface IFilterItem {
    main: boolean,
    title: string,
    value: string,
}

export interface IMetaInfo {
    current_page: number
    from: number
    last_page: number
    path: string
    per_page: number
    to: number
    total: number
}
