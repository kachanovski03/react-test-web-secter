import React, {ReactNode} from 'react';
import styles from "./styles.module.css"

interface IProps {
    label: string
    children: ReactNode
}

export const FilterWithLabel = ({label, children}: IProps) => {
    return (
        <div className={styles.wrapper}>
            <label className={styles.label}>
                {label}
            </label>
            {children}
        </div>
    );
};





