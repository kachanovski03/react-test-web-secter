import {instance} from "./instance"

export const catalogAPI = {
    getCatalog(category: string, queryUrl: string = "") {
        return instance.get(`${category}?${queryUrl}`).then(res => res.data)
    },
}
