import axios from "axios";

export const instance = axios.create({
    withCredentials: true,
    baseURL: "https://getlens-master.stage.dev.family/api/pages/",
});
