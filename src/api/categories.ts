import {instance} from "./instance"

export const categoriesAPI = {
    getCategories() {
        return instance.get(`/`).then(res => res.data)
    },
}
