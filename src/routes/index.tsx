import React from 'react';
import {Route, Routes} from "react-router-dom";
import {CatalogScreen, MainScreen, NotFound} from '../pages';

export const Pages = () => {

    return (
        <Routes>
            <Route path="/" element={<MainScreen/>}/>
            <Route path="/catalog/:category" element={<CatalogScreen/>}/>
            <Route path="*" element={<NotFound/>}/>
        </Routes>
    );
};

