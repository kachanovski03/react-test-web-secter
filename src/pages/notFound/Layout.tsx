import React from 'react';
import { Link } from 'react-router-dom';
import styles from './styles.module.css'

export const Layout = () => {
    return (
        <div className={styles.notFound}>
            <span className={styles.text}>
                 Запрашиваемая вами страница не найдена
            </span>
            <Link to={'/'} className={styles.link}>На главную</Link>
        </div>
    );
};
