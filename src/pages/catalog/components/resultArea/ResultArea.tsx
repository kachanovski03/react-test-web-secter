import React from 'react';
import { ProductCard } from '../../../../compoennts';
import { IItem } from '../../../../interface';
import styles from "./styles.module.css"

interface IProps {
    items: IItem[]
}

export const ResultArea = ({items}: IProps) => {
    return (
        <div className={styles.resultArea}>
            {items?.map(item => <ProductCard key={item.id} item={item} />) ?? "net tovarov"}
        </div>
    );
};
