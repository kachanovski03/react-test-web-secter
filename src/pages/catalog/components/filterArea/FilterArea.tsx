import React from 'react';
import {Checkbox, PriceRange} from '../../../../compoennts';
import {FILTER_TYPE} from '../../../../constants';
import {FilterWithLabel} from '../../../../hoc';
import {IFilter, IMetaInfo} from '../../../../interface';
import styles from './styles.module.css'

interface IProps {
    filters: IFilter[]
    pageMetaInfo: IMetaInfo | null
    changeFilterValue: (url: string) => void
    category?: string
}

export const FilterArea = ({filters,category, pageMetaInfo, changeFilterValue}: IProps) => {
    const handleChange = (event: React.ChangeEvent<HTMLFormElement>) => {
        let form = event.currentTarget;
        let formData = new FormData(form);
        const data = [...formData.entries()];
        const url = data
            .filter(x => x[1])
            .map(i => `${i[0]}=${i[1]}`).join("&")
        const encodeUrl = new URLSearchParams(url).toString();
        changeFilterValue(encodeUrl)
    }

    const renderFilter = (filter: IFilter) => {
        switch (filter.type) {
            case FILTER_TYPE.RANGE:
                return <PriceRange id={filter.slug}/>
            case FILTER_TYPE.MULTIPLE:
                return filter.items.map(i => {
                        return <Checkbox
                            value={i.value}
                            key={i.value}
                            label={i.title}
                            id={i.value}
                            name={`${filter.slug}[]`}
                        />
                    }
                )
            case FILTER_TYPE.CHECKBOX:
                return <Checkbox
                    key={filter.slug}
                    label={filter.title}
                    id={filter.slug}
                    name={`${filter.slug}[]`}
                />
            default:
                return null
        }
    }

    return (
        <div className={styles.filterArea}>
            <div className={styles.itemsCount}>
                {
                    pageMetaInfo?.total || pageMetaInfo?.total !== 0
                        ? `Товаров ${pageMetaInfo?.total}`
                        : `Товаров не найдено`
                }
            </div>
            <h1 className={styles.categoryName}>
                {category}
            </h1>
            <form name={"catalog-filters"} onChange={handleChange}>
                {filters?.map(filter =>
                    <FilterWithLabel key={filter.slug} label={filter.title}>
                        {renderFilter(filter)}
                    </FilterWithLabel>
                )}
            </form>
        </div>
    );
};
