import {FilterArea} from "./filterArea/FilterArea";
import {ResultArea} from "./resultArea/ResultArea";

export {
    FilterArea, ResultArea
}
