import React from 'react';
import styles from "./styles.module.css"
import {FilterArea, ResultArea} from "./components";
import {IFilter, IItem, IMetaInfo} from "../../interface";

interface IProps {
    items: IItem[]
    filters: IFilter[]
    pageMetaInfo: IMetaInfo | null
    changeFilterValue: (url: string) => void
    category?: string
}

export const Layout = (props: IProps) => {
    const {items, filters, pageMetaInfo, category, changeFilterValue} = props
    return (
        <div className={styles.catalog}>
            <FilterArea category={category} changeFilterValue={changeFilterValue} pageMetaInfo={pageMetaInfo}
                        filters={filters}/>
            <ResultArea items={items}/>
        </div>
    );
};
