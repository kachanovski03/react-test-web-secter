import React, {useEffect} from 'react';
import {Layout} from "./Layout";
import {useDispatch, useSelector} from "react-redux";
import {RootStateType} from "../../redux/store";
import {getCatalogData} from "../../redux/thunks";
import {useNavigate, useParams} from "react-router-dom";
import {catalogActions} from "../../redux/actions";
import {ICatalogReducer} from "../../redux/reducers";

export const Container = () => {
    const navigate = useNavigate();
    const {category} = useParams();
    const dispatch = useDispatch()
    const {
        products,
        filters,
        meta,
        currentCategory
    } = useSelector<RootStateType, ICatalogReducer>(state => state.catalog)

    useEffect(() => {
        if (category) {
            dispatch(getCatalogData(category))
        }
        return () => {
            dispatch(catalogActions.setCatalogFilters([]))
            dispatch(catalogActions.setCatalogMeta(null))
            dispatch(catalogActions.setCurrentCategory(null))
            dispatch(catalogActions.setCatalogCategories([]))
        }
    }, [category, dispatch])

    const changeFilterValue = async (url: string) => {
        navigate({
            search: url
        })
        category && dispatch(getCatalogData(category, url))
    }

    return (
        <Layout
            category={currentCategory?.title}
            changeFilterValue={changeFilterValue}
            items={products}
            filters={filters}
            pageMetaInfo={meta}
        />
    );
};
