import React, {useEffect} from 'react';
import {Layout} from "./Layout";
import {useDispatch, useSelector} from "react-redux";
import {RootStateType} from "../../redux/store";
import {ICategory} from "../../interface";
import {getCategories} from "../../redux/thunks/categories";

export const Container = () => {
    const dispatch = useDispatch()
    const categories = useSelector<RootStateType, ICategory[]>(state => state.categories.categories)
    useEffect(() => {
        dispatch(getCategories())
    }, [dispatch])
    return (
        <Layout categories={categories}/>
    );
};
