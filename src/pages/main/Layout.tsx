import React from 'react';
import styles from './styles.module.css'
import {ICategory} from "../../interface";
import {CategoryCard} from "./components";

interface IProps {
    categories: ICategory[]
}

export const Layout = ({categories}: IProps) => {

    return (
        <div className={styles.main}>
            {categories.map(category => <CategoryCard key={category.slug} category={category} />)}
        </div>
    );
};
