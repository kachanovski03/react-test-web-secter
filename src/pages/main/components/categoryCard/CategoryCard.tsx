import React from 'react';
import styles from "./styles.module.css"
import {Link} from "react-router-dom";
import {ICategory} from "../../../../interface";
import noPhoto from '../../../../assets/images/no-photo.png'

interface IProps {
    category: ICategory
}

export const CategoryCard = ({category}: IProps) => {
    return (
        <div className={styles.card}>
            <Link className={styles.link} to={`/catalog/${category.slug}`}>
                <img className={styles.image} alt={'category'} src={category.image?.desktop.webp_x1 ?? noPhoto}/>
                <span className={styles.text}>{category.title}</span>
            </Link>
        </div>
    );
};
