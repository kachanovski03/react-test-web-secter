import React, {ChangeEvent, DetailedHTMLProps, InputHTMLAttributes, useEffect, useState} from 'react';
import styles from "./styles.module.css"

interface IProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    label: string
    value?: string
    id: string
    name: string
}

export const Checkbox = ({label, id, defaultValue, value, name, ...props}: IProps) => {
    const [checked, setChecked] = useState<boolean>(false)
    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
        setChecked(e.target.checked)
    }

    return (
        <>
            <input type='checkbox'
                   value={value ?? String(!checked)}
                   name={name}
                   checked={checked}
                   onChange={onChange}
                   className={styles.checkbox}
                   id={id}
                   {...props}
            />
            <label className={styles.label} htmlFor={id}>{label}</label>
        </>
    );
};
