import React, {ButtonHTMLAttributes, DetailedHTMLProps, ReactNode} from 'react';
import styles from "./styles.module.css"
import cn from "classnames"

interface IProps extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    children: ReactNode,
    appearance: 'primary' | "ghost",
}

export const Button = ({children, appearance, className, ...props}: IProps) => {
    return (
        <button className={
            cn(styles.button, {
                [styles.primary]: appearance === "primary",
                [styles.ghost]: appearance === "ghost",
            })}
                {...props}
        >
            {children}
        </button>
    );
};
