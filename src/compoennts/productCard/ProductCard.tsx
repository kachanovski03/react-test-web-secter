import React from 'react';
import styles from './styles.module.css'
import {ReactComponent as Favorite} from '../../assets/icons/Group 132.svg'
import {IItem} from "../../interface";
import {Button} from "../button/Button";

interface IProps {
    item: IItem
}

export const ProductCard = ({item}: IProps) => {
    return (
        <div className={styles.card}>
            {item.is_new && <div className={styles.newLabel}>Новое</div>}
            <img alt={"item_img"} className={styles.image} src={item.image.desktop.webp_x1}/>
            <div className={styles.infoWrapper}>
                <div className={styles.name}>
                    {item.title}
                </div>
                <div className={styles.priceWrapper}>
                    <span className={styles.price}>
                       {item.price}
                    </span>
                    {!item.is_second_hand && <span className={styles.isNew}>
                       Новое
                    </span>}
                </div>
                <div className={styles.actions}>
                    <Button appearance={"ghost"} className={styles.button}>
                        В корзину
                    </Button>
                    <Favorite className={styles.like}/>
                </div>
            </div>
        </div>
    );
};
