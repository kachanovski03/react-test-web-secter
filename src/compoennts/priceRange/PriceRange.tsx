import React, {ChangeEvent, useState} from 'react';
import styles from './styles.module.css'

interface IProps {
    id?: string
}

export const PriceRange = ({id}: IProps) => {

    const [minValue, setMinValue] = useState<string>("")
    const [maxValue, setMaxValue] = useState<string>("")

    const onChangeMinValue = (e: ChangeEvent<HTMLInputElement>) => {
        const min = e.currentTarget.value
        setMinValue(min)
    }
    const onChangeMaxValue = (e: ChangeEvent<HTMLInputElement>) => {
        const max = e.currentTarget.value
        setMaxValue(max)
    }

    return (
        <div className={styles.priceRange}>
            <input name={`${id}[min]`} value={minValue} onChange={onChangeMinValue} type={"number"}
                   className={styles.priceInput}/>
            <input name={`${id}[max]`} value={maxValue} onChange={onChangeMaxValue} type={"number"}
                   className={styles.priceInput}/>
        </div>
    );
};
